package com.demo.tools;

import java.io.File;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

public class IBMCloudRobot {

  private static final String DRIVER_PATH = "/Users/kitechen/Downloads/chromedriver";

  private static final String TARGET_URL = "https://cloud.ibm.com/login";

  private static final String DOWNLOAD_PATH = "/Users/kitechen/Downloads/intents";

  private static final String INTENT_PATH =
      "/Users/kitechen/Downloads/intents/router_%s_intents.csv";

  private static final String BANK_INTENT_URL =
      "https://assistant-us-south.watsonplatform.net/rest/content/workspaces/73d67b89-e33f-4521-bda8-850b36a73d4d/intents?all=true";

  private static final String ORDER_INTENT_URL =
      "https://assistant-us-south.watsonplatform.net/rest/content/workspaces/069c5e2b-ff47-477f-a4de-9d50155629c6/intents?all=true";

  private static final String USERNAME = "";

  private static final String SECRET = "";

  private static final String ASSIST_SERVICE_NAME = "Watson Assistant-xj";

  private static final String ROUTER_NAME = "router";

  private static final String[] targetSkills = {"order", "bank"};

  private final WebDriver driver;

  private final Wait<WebDriver> wait;

  public IBMCloudRobot() {
    ChromeDriverService service = new ChromeDriverService.Builder()
        .usingDriverExecutable(new File(DRIVER_PATH)).usingAnyFreePort().build();

    Map<String, Object> prefs = new HashMap<>();
    prefs.put("credentials_enable_service", false);
    prefs.put("profile.password_manager_enabled", false);
    prefs.put("download.default_directory", DOWNLOAD_PATH);

    ChromeOptions options = new ChromeOptions();
    options.addArguments("--start-maximized");
    options.addArguments("test-type");
    options.addArguments("disable-extensions");
    options.setExperimentalOption("prefs", prefs);
    driver = new ChromeDriver(service, options);
    wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(60))
        .pollingEvery(Duration.ofSeconds(1)).ignoring(NoSuchElementException.class);
  }

  public void login() {
    driver.get(TARGET_URL);
    WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("userid")));

    String loginCss =
        "body > main > div.ibm-cloud-app.ibm-cloud-react-container > div > div.login-container > div.login-wrapper > form > div.login-form__login-button-wrapper > div:nth-child(2) > button";
    element.sendKeys(USERNAME);
    driver.findElement(By.cssSelector(loginCss)).click();

    element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("password")));
    element.sendKeys(SECRET);
    driver.findElement(By.cssSelector(loginCss)).click();

    System.out.println("logining");
  }

  public void download() {
    String skillTabCss =
        "body > section > div > div > main-menu > div > nav > ul > li:nth-child(2) > a";
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(skillTabCss)));
    // wait for download and prevent BOT detect
    try {
      Thread.sleep(1000);
      System.out.println("suspending 1");
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    
    System.out.println(BANK_INTENT_URL);
    driver.get(BANK_INTENT_URL);
    System.out.println(ORDER_INTENT_URL);
    driver.get(ORDER_INTENT_URL);
  }

  public void close() {
    driver.quit();
  }

  public void accessSkills() {
    String skillTabCss =
        "body > section > div > div > main-menu > div > nav > ul > li:nth-child(2) > a";
    String skillCardCssLike = "div[id*='SkillCard-']";
    String circleXPath = "//*[@id=\"instance-loading\"]";

    //TODO really spinning icon xpath ?
    wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(circleXPath)));
    System.out.println("loading 1");

    WebElement skillsTabElem =
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(skillTabCss)));
    // workaround: waiting for loading icon go away
    try {
      Thread.sleep(5000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    skillsTabElem.click();

    // check for each skill div shown
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(skillCardCssLike)));
    System.out.println("switching to skill list");
  }

  public void accessAssist() {
    // keep old handle for later new tab check
    Set<String> oldHandles = driver.getWindowHandles();

    String resourceXPath = "//*[@id=\"main-content\"]/div/div/div[2]/div[2]/div/div[1]/div[1]/a";
    this.waitUtilClick(resourceXPath);
    System.out.println("access service list");

    String assistantXPath = String.format("//div[contains(text(), '%s')]", ASSIST_SERVICE_NAME);
    this.waitUtilClick(assistantXPath);
    System.out.println("access assistant");

    String initAssXPath =
        "//*[@id=\"main-content\"]/div/div[3]/div[6]/div/div/section/div[1]/div/div/div/div[1]/div[2]/a[1]";
    this.waitUtilClick(initAssXPath);
    System.out.println("click assistant init");

    wait.until(ExpectedConditions.numberOfWindowsToBe(2));
    Set<String> allHandles = driver.getWindowHandles();
    for (String handle : allHandles) {
      if (!oldHandles.contains(handle)) {
        driver.switchTo().window(handle);
      }
    }
    System.out.println("switching to new assistant: " + ASSIST_SERVICE_NAME);
  }

  public void importIntents() throws InterruptedException {
    
    this.accessSkills();
    
    String skillXPath = String.format("//div[contains(text(), '%s')]", ROUTER_NAME);
    this.waitUtilClick(skillXPath);
    System.out.println("entering router");

    String intentImportXPath =
        "//*[@id=\"build-view\"]/intents/div/div[5]/div[1]/div/section/div[2]/div/button[2]";
    String importInputXPath =
        "//*[@id=\"build-view\"]/intents/div/div[5]/div[3]/div/div[2]/div/input";
    String importBtnXPath =
        "//*[@id=\"build-view\"]/intents/div/div[5]/div[3]/div/div[3]/button[2]";
    String doneBtnXPath = "//button[contains(text(), 'Done')]";

    for (int i = 0; i < targetSkills.length; i++) {
      this.waitUtilClick(intentImportXPath);
      driver.switchTo().activeElement();
      System.out.println("popup importing modal: " + targetSkills[i]);

      this.waitUtilSendKey(importInputXPath, String.format(INTENT_PATH, targetSkills[i]));
      this.waitUtilClick(importBtnXPath);

      System.out.println("importing skill intents");
      Thread.sleep(1000);

      wait.until(new ExpectedCondition<Boolean>() {
        public Boolean apply(WebDriver driver) {
          driver.switchTo().activeElement();
          boolean btnIsEmpty = driver.findElements(By.xpath(doneBtnXPath)).isEmpty();
          System.out.println("checking done button size: " + btnIsEmpty);
          return btnIsEmpty;
        }
      });

      this.waitUtilClick(doneBtnXPath);
    }
  }

  public void exportIntents() throws InterruptedException {
    String intentExprotXPath =
        "//*[@id=\"build-view\"]/intents/div/div[5]/div[1]/div/section/div[2]/div/button[3]";

    for (int i = 0; i < targetSkills.length; i++) {

      String skillXPath = String.format("//div[contains(text(), '%s')]", targetSkills[i]);
      this.waitUtilClick(skillXPath);
      this.waitUtilClick(intentExprotXPath);

      System.out.println("exporting skill intents");
      Thread.sleep(1000);

      // for next skill and importing to router
      this.accessSkills();
    }
  }

  private void waitUtilClick(String xPath) {
    WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPath)));
    element.click();
  }

  private void waitUtilSendKey(String xPath, String sendKey) {
    WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPath)));
    element.sendKeys(sendKey);
  }
}
