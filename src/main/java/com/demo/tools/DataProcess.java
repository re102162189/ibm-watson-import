package com.demo.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class DataProcess {

  private static final String FILE_PATH = "/Users/kitechen/Downloads/intents/%s_intents.csv";

  private static final String ROUTER_FILENAME = "router_%s";

  private Map<String, String> skillMap = new HashMap<>();

  public DataProcess() {
    skillMap.put("order", "069c5e2b-ff47-477f-a4de-9d50155629c6");
    skillMap.put("bank", "73d67b89-e33f-4521-bda8-850b36a73d4d");
  }

  public void convert() {
    for (Map.Entry<String, String> skill : skillMap.entrySet()) {
      String skillName = skill.getKey();
      String skillId = skill.getValue();
      
      String intentPath = String.format(FILE_PATH, skillId);
      String routerFileName = String.format(ROUTER_FILENAME, skillName);
      String routerPath = String.format(FILE_PATH, routerFileName);
      StringBuilder intents = new StringBuilder();
      
      try (BufferedReader br =
          new BufferedReader(new FileReader(intentPath))) {
        System.out.println(intentPath);
        String line;
        while ((line = br.readLine()) != null) {
          int endIndex = line.lastIndexOf(',');
          if (endIndex != -1) {
            String repalceInent = line.substring(0, endIndex + 1) + skillName;
            intents.append(repalceInent).append("\n");
          }
        }
      } catch (IOException e) {
        e.printStackTrace();
      }

      try (BufferedWriter bw =
          new BufferedWriter(new FileWriter(routerPath))) {
        bw.write(intents.toString());
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
}
