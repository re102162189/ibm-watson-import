package com.demo.tools;

public class Main {
  public static void main(String[] args) throws InterruptedException {
    IBMCloudRobot iRobot = new IBMCloudRobot();
    iRobot.login();
    iRobot.accessAssist();
    iRobot.download();
    
    //not stable steps
    //iRobot.accessSkills();
    //iRobot.exportIntents();

    try {
      Thread.sleep(5000);
      System.out.println("suspending 2");
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    
    DataProcess dataProcess = new DataProcess();
    dataProcess.convert();
    
    //TODO
    iRobot.importIntents();
    iRobot.close();
  }
}
